import random
import sys
import thread
import web
try:
    import cPickle as pickle
except ImportError:
    import pickle

class Task(object):
    def __init__(self,code):
        self.id = random.randint(0,999999)
        self.code = code
        self.results = []

    def add_result(self,solution):
        self.results.append(solution)

    def __repr__(self):
        str = "id=%s, code=%s, len(results)=%s" % \
            (self.id, self.code, len(self.results))
        for s in self.results:
            str += "\n\t=> %s" % s
        return str

class Storage(object):
    @staticmethod
    def from_file(filename):
        s=pickle.load(filename)
        if isinstance(s, Storage):
            return s
        else:
            raise Exception("Bad file: %s" % filename)

    def __init__(self):
        self.unsolved = []
        self.waiting = []
        self.solved = []
    
    def getOneUnsolved(self):
        if self.unsolved.__len__() > 0:
            task = self.unsolved.pop()
            self.waiting.append(task)
        else:
            task = None
        return task

    def putSolution(self,id,result):
        for task in self.waiting:
            if task.id == id:
                task.add_result(result)
                self.solved.append(task)
                self.waiting.remove(task)

    def save(self,filename):
        pickle.dump(self, open(filename,'w'))

urls = (
    '/', 'index',
    '/jobs', 'jobs',
    '/library.js', 'library'
    )

class library:
    library = ""
    def GET(self):
        web.header("Content-Type","text/javascript; charset=utf-8")
        print self.library

    @staticmethod
    def add(code):
        library.library += "\n" + code

class index:
    def GET(self):
        index = web.template.Template(open("templates/index.tmpl").read())
        print index()

class jobs:
    storage = Storage()
    def GET(self):
        task = self.storage.getOneUnsolved()
        if task:
            print "%s=%s" % (task.id, task.code)
        else:
            print ""

    def POST(self):
        post = web.input("id", "result")
        self.storage.putSolution(int(post.id), post.result)

def profiler(app):
    """Outputs basic profiling information at the bottom of each response."""
    import profile
    def profile_internal(e, o):
        out, result = web.profile(app)(e, o)
        sys.stdout.write( str(out) + "\n" + str(result) + "\n" )
        return out
    return profile_internal


def run(threaded=False):
    web.webapi.internalerror = web.debugerror
    if threaded:
#        thread.start_new_thread(web.run,(urls,globals(),profiler))
        thread.start_new_thread(web.run,(urls,globals()))
    else:
        web.run(urls, globals())


if __name__ == "__main__": 
    thread.start_new_thread(run,())
    while True:
        line = sys.stdin.readline().strip()
        if line == "exit": sys.exit(0)
        elif line == "": print jobs.storage
        else:
            jobs.storage.add(Task(line));
            print "Pushed: %s" % line
