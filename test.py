import threading

class JSQueue:
    def __init__(self):
        self.lock = threading.Condition()
        self.queue = list()
    
    def enqueue(self,call):
        try:
            self.lock.acquire()
            self.queue.append(call)
            self.lock.notify()
        finally:
            self.lock.release()
    def dequeue(self):
        try:
            self.lock.acquire()
            
        finally:
            self.lock.release()
                

class JSFunctionCall(object):
    def __init__(self):
        self.lock = threading.Lock()
        self.result = None)

    def free(self, result):
        self.result = result
        
    def wait(self):

class JSFunctionFactory(object):
    def __init__(self,queue,server):
        self.queue = queue
        self.server = server

    def createFunction(self,args,code,name):
        return JSFunction(args,code,name,self.queue,self.server)

class JSFunction(cached=False):
    def __init__(self,args,code,name):
        self.args = args
        self.name = name
        self.code = code
        self.cache = {}

    def getDefinition(self):
        return "function %s {\n%s}\n" % (self.name, self.code)

    def __call__(self,*args):
        print args,kwargs

q  = JSQueue()
s  = AnyServer(q)
ff = JSFunctionFactory(q,s)

dreieck = ff.createFunction(['a','b'],"""
return Math.sqrt(Math.pow(a,2) + Math.pow(b,2));
""", "dreieck")

for i in range(3,15):
    print "a=%s,b=%s => c=%s" % (i, i*2, dreieck(i, i*2))
