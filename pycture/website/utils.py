
import string


auth_chars = string.ascii_letters + string.digits + string.punctuation
def filter_auth(s):
	new = [c for c in s if c in auth_chars]
	new = string.join(new, '')
	return new

def filter_hex(s):
	new = [c for c in s if c in string.hexdigits]
	new = string.join(new, '')
	new = new.lower()
	return new

def filter_file(s):
	new = s.replace('/', '')
	return new

