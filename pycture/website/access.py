#coding: utf8

import web
import sessions
import utils

from config import users, admins


def auth(user, passwd):
	if user in users and users[user] == passwd:
		return True
	if user in admins and admins[user] == passwd:
		return True
	return False


def get_user():
	sid = web.cookies(sid = None)['sid']
	if not sid:
		return None
	sid = utils.filter_hex(sid)
	s = sessions.get(sid)
	if not s:
		return None
	return s['username']

def get_sid():
	sid = web.cookies(sid = None)['sid']

