#coding: utf8

import os
import pickle
import time
import threading

import Image as PILImage

import EXIF
import config


# Broken image exception
class BrokenImageExc (Exception):
	pass


# decorator for simplified locking
# we only lock what would cause image damage or metadata loss, it's not an
# exhaustive locking because it's not worth the trouble
def withlock(f):
	def newf(self, *args, **kwargs):
		self.lock.acquire()
		try:
			return f(self, *args, **kwargs)
		finally:
			self.lock.release()
	return newf

# check if the first file is newer than the second
def isnewerthan(f1, f2):
	t1 = os.stat(f1).st_mtime
	t2 = os.stat(f2).st_mtime
	if t1 > t2:
		return True
	return False



class Metadata (object):
	def __init__(self, path, defaults):
		self.path = path
		self.dict = None
		self.defaults = defaults
		self.lock = threading.Lock()

	@withlock
	def save(self):
		pickle.dump(self.dict, open(self.path, 'w'),
				protocol = pickle.HIGHEST_PROTOCOL)

	@withlock
	def _load(self):
		if os.path.isfile(self.path):
			d = pickle.load(open(self.path))
			# fill in for the missing values
			for k, v in self.defaults.iteritems():
				if k not in d:
					d[k] = v
			self.dict = d
			return
		self.dict = self.defaults

	def _get_md(self):
		if self.dict is None:
			self._load()
		return self.dict
	mdata = property(fget = _get_md)

	def __getattr__(self, name):
		if name in self.mdata.keys():
			return self.mdata[name]
		else:
			raise AttributeError, 'No such attribute: ' + name

	def __setattr__(self, name, value):
		if name in ['path', 'dict', 'defaults', 'lock']:
			return object.__setattr__(self, name, value)
		self.lock.acquire()
		try:
			if name in self.mdata.keys():
				self.mdata[name] = value
				return
			else:
				raise AttributeError, 'No such attribute: ' + name
		finally:
			self.lock.release()


class Exif (object):
	def __init__(self, path):
		self.path = path
		self.camera = ''
		self.date = ''
		self.orientation = ''
		self.flash = ''
		self.exposure = ''
		self.iso = ''
		self.focallenght = ''
		self.fnumber = ''
		self._have_data = False
		self._load()

	def _load(self):
		try:
			tags = EXIF.process_file(open(self.path))
		except:
			# EXIF.py breaks for some photos :(
			return

		if tags.get('Image Make'):
			self.camera += str(tags['Image Make'])
		if tags.get('Image Model'):
			self.camera += ' ' + str(tags['Image Model'])
		self.camera = self.camera.strip()

		t = str(tags.get('Image DateTime', ''))
		if t:
			try:
				tup = time.strptime(t, "%Y:%m:%d %H:%M:%S")
				rep = time.strftime('%Y-%m-%d %H:%M:%S', tup)
				self.date = rep
			except:
				pass

		self.orientation = str(tags.get('Image Orientation', '')).strip()
		self.flash = str(tags.get('EXIF Flash', '')).strip()
		self.exposure = str(tags.get('EXIF ExposureTime', '')).strip()
		self.iso = str(tags.get('EXIF ISOSpeedRatings', '')).strip()

		self.focallength = str(tags.get('EXIF FocalLength', '')).strip()
		if '/' in self.focallength:
			p, q = self.focallength.split('/')
			self.focallength = str(float(p) / float(q))

		self.fnumber = str(tags.get('EXIF FNumber', '')).strip()
		if '/' in self.fnumber:
			p, q = self.fnumber.split('/')
			self.fnumber = str(float(p) / float(q))


		self._have_data = self.camera or self.date \
				or self.orientation or self.flash \
				or self.exposure or self.iso \
				or self.focallength or self.fnumber

	def has_data(self):
		return self._have_data


class Comment (object):
	def __init__(self):
		self.date = ''
		self.author = ''
		self.content = ''

class Comments (object):
	def __init__(self, fname):
		self.fname = fname
		self.lock = threading.Lock()

	@withlock
	def get(self):
		# we assume the amount of comments is small enough to do this,
		# and that it's not worth to cache the whole thing
		clist = []
		current = None
		if not os.path.isfile(self.fname):
			return []
		for l in open(self.fname):
			if l == '--- NEW COMMENT ---\n':
				if current:
					clist.append(current)
				current = Comment()
			elif l.startswith('C-Author:'):
				f = l.split(':', 1)[1].strip()
				current.author = f
			elif l.startswith('C-Date:'):
				f = l.split(':', 1)[1].strip()
				current.date = f
			else:
				current.content += l
		if current:
			clist.append(current)
		return clist

	@withlock
	def append(self, author, content):
		fd = open(self.fname, 'a')
		fd.write('--- NEW COMMENT ---\n')
		fd.write('C-Date: %s\n' % time.strftime('%Y-%m-%d %H:%M:%S'))
		fd.write('C-Author: %s\n' % author)
		fd.write(content + '\n')
		fd.close()

	@withlock
	def isempty(self):
		if os.path.isfile(self.fname):
			return False
		return True


class Image (object):
	def __init__(self, album, fname):
		self.album = album
		self.fname = fname
		self.number = 0
		self.lock = threading.Lock()
		self.path = album.imagespath + '/' + fname
		self.thumbpath = album.thumbpath + '/' + fname
		self.mediumpath = album.mediumpath + '/' + fname
		self.mdatapath = album.mdatapath + '/' + fname
		self.mdata = Metadata(self.mdatapath,
				defaults = {
					'caption': '',
					'desc': ''
				} )
		self.comments = Comments(album.commentspath + '/' + fname)
		self._format = None
		self._size = None
		self._exif = None

	def open(self):
		return open(self.path)

	def get_format(self):
		if self._format != None:
			return self._format
		self._format = PILImage.open(self.path).format
		return self._format
	format = property(fget = get_format)

	def get_size(self):
		if self._size != None:
			return self._size
		self._size = PILImage.open(self.path).size
		return self._size
	def set_size(self, newsize):
		self._size = newsize
	size = property(fget = get_size, fset = set_size)

	def get_exif(self):
		if self._exif != None:
			return self._exif
		self._exif = Exif(self.path)
		return self._exif
	exif = property(fget = get_exif)

	@withlock
	def openthumb(self):
		if os.path.isfile(self.thumbpath) and \
				isnewerthan(self.thumbpath, self.path):
			tim = PILImage.open(self.thumbpath)
			if tim.size[0] == config.thumbsize[0] or \
					tim.size[1] == config.thumbsize[1]:
				tim.fp.seek(0)
				return tim.fp
		im = PILImage.open(self.path).copy()
		im.thumbnail(config.thumbsize, PILImage.ANTIALIAS)
		im.save(self.thumbpath)
		return open(self.thumbpath)

	@withlock
	def openmedium(self):
		if os.path.isfile(self.mediumpath) and \
				isnewerthan(self.mediumpath, self.path):
			mim = PILImage.open(self.mediumpath)
			if mim.size[0] == config.mediumsize[0] or \
					mim.size[1] == config.mediumsize[1]:
				mim.fp.seek(0)
				return mim.fp
		im = PILImage.open(self.path).copy()
		im.thumbnail(config.mediumsize, PILImage.ANTIALIAS)
		im.save(self.mediumpath)
		return open(self.mediumpath)

	@withlock
	def rotate(self, times = 1):
		# times is the number of times to rotate the image 90 degrees
		# counter-clockwise
		# We can't use im.rotate() because it won't resize the image;
		# future versions of PIL (> 1.1.5) will have an "expand"
		# argument that we can use, but until then we're stucked with
		# repeated transposes
		rot = PILImage.open(self.path)
		while times:
			rot = rot.transpose(PILImage.ROTATE_90)
			times -= 1
		rot.save(self.path)
		self.size = rot.size


class Album (object):
	def __init__(self, name, path, parent = None):
		self.name = name
		self.path = path
		self.parent = parent
		if parent:
			self.fname = parent.fname + '/' + name
		else:
			self.fname = name

		self.lock = threading.Lock()
		self.images = {}
		self.imageslist = []
		self.count = 0
		self.featured = None
		self.children = {}
		self.childrenlist = []
		self.users = {}
		self.mdata = Metadata(self.path + '/album_mdata',
				defaults = {
					'caption': '',
					'desc': '',
					'featured': '',
				} )

		self.imagespath = self.path + '/images'
		self.thumbpath = self.path + '/thumbnails'
		self.mediumpath = self.path + '/medium'
		self.mdatapath = self.path + '/mdata'
		self.childrenpath = self.path + '/children'
		self.commentspath = self.path + '/comments'

		self._load_images()
		self._load_children()
		self._load_users()

	def _load_images(self):
		count = 0
		imgs = os.listdir(self.imagespath)
		for f in imgs:
			# We use extensions to determine which one are images.
			# It's lame, I know, but it's the fastest way.
			lc = f.lower()
			if lc.endswith('.jpg') or \
					lc.endswith('.png') or \
					lc.endswith('.gif') or \
					lc.endswith('.tif'):
				count += 1
				try:
					self.images[f] = Image(self, f)
				except BrokenImageExc:
					count -= 1
					continue
		self.count = count
		self.imageslist = self.images.keys()
		self.imageslist.sort()
		if self.mdata.featured and self.mdata.featured in self.images:
			self.featured = self.images[self.mdata.featured]

		c = 1
		for i in self.imageslist:
			self.images[i].number = c
			c += 1


	def list_images(self):
		return [ self.images[i] for i in self.imageslist ]

	def get_image(self, name):
		return self.images[name]

	@withlock
	def get_featured_image(self, user):
		if self.featured:
			return self.featured
		elif len(self.images):
			return self.images[self.imageslist[0]]
		else:
			c = self.children_allowed(user)
			if not c:
				return None
			return c[0].get_featured_image(user)

	@withlock
	def set_featured_image(self, name):
		if name not in self.imageslist:
			return
		self.mdata.featured = name
		self.mdata.save()
		self.featured = self.images[name]

	def _load_children(self):
		if not os.path.isdir(self.childrenpath):
			return []

		children = os.listdir(self.childrenpath)
		children.sort()
		l = []
		cdict = {}
		for d in children:
			fullpath = self.childrenpath + '/' + d
			if os.path.isdir(fullpath):
				a = Album(d, fullpath, parent = self)
				l.append(a)
				cdict[d] = a
		self.childrenlist = l
		self.children = cdict

	def _load_users(self):
		if not os.path.isfile(self.path + '/users'):
			return

		f = open(self.path + '/users')
		for u in f:
			l = u.strip().rsplit(None, 1)
			if not l:
				continue
			if len(l) > 1:
				u, p = l
				u, p = u.strip(), p.strip()
				if 'a' in p:
					p = 'rwa'
				elif 'w' in p:
					p = 'rw'
				else:
					p = 'r'
				self.users[u] = p
			else:
				u = l[0].strip()
				self.users[u] = 'r'

	def allowed(self, user):
		if self.parent:
			if not self.parent.allowed(user):
				return ''
		if user in config.admins:
			return 'rwa'
		if user in self.users.keys():
			return self.users[user]
		if 'all' in self.users.keys():
			return self.users['all']
		return ''

	def children_allowed(self, user):
		l = []
		for c in self.childrenlist:
			if c.allowed(user):
				l.append(c)
		return l

	def get_mtime(self):
		return os.stat(self.imagespath).st_mtime
	mtime = property(fget = get_mtime)

	def get_strmtime(self):
		return time.strftime("%d %b, %Y", time.localtime(self.mtime))
	strmtime = property(fget = get_strmtime)

	def get_title(self):
		if self.mdata.caption:
			return self.mdata.caption
		return self.name
	title = property(fget =  get_title)


alist = []
def list_albums():
	if alist:
		return alist
	l = os.listdir(config.albumspath)
	for d in l:
		if os.path.isdir(config.albumspath + '/' + d + '/images'):
			alist.append(d)
	alist.sort()
	return alist

def get_album_list(user):
	albums = [ get_album(a, user) for a in list_albums() ]
	albums = [ a for a in albums if a is not None ]
	return albums

def list_all_albums(user, album = None):
	l = []
	if album:
		c = [ i.fname for i in album.childrenlist if i.allowed(user)]
		l.extend(c)
		for i in album.childrenlist:
			l.extend(list_all_albums(user, i))
	else:
		for i in get_album_list(user):
			l.append(i.fname)
			l.extend(list_all_albums(user, i))
	l.sort()
	return l

def get_all_album_list(user):
	albums = [ get_album(a, user) for a in list_all_albums(user) ]
	albums = [ a for a in albums if a is not None ]
	return albums


albums = {}
def get_album(name, user):
	if name in albums:
		a = albums[name]
		if not a.allowed(user):
			return None
		return a

	if '..' in name:
		raise

	if '/' in name:
		pname, cname = os.path.split(name)
		if not cname:
			return None
		parent = get_album(pname, user)
		if not parent:
			return None
		if cname not in parent.children:
			return None
		a = parent.children[cname]
		albums[name] = a
		if not a.allowed(user):
			return None
		return a
	else:
		# root album
		a = Album(name, config.albumspath + '/' + name)
		albums[name] = a
		if not a.allowed(user):
			return None
		return a

def reload_albums():
	# FIXME: check locking on this
	global alist
	global albums
	alist = []
	albums = {}

