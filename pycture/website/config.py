
# Size of the thumbnail and detailed views.
# To use these sizes it's necessary to browse with at least 1280x1024.
#
# If you change them, keep them squared, and with a similar ratio.
# The recommended rule is to use (min_resolution * 0.2) for thumbsize and
# (min_resolution * 0.5) for mediumsize.
thumbsize = 256, 256
mediumsize = 600, 600

# Path to the albums.
albumspath = '/var/albums'

# Number of images to show per page.
imagesxpage = 15

# Dictionary of users and their passwords.
users = {
	'me': 'mysecretpassword',
	'user1': 'password1',
}
"""_users = DictAuthenticator(users={
        'me' : {
            'password' : 'iii',
            'roles':'admin,user'
            },
        'he' : {
            'password' : 'bla',
            'roles':'user'
            }
      })
"""
# Dictionary of admins and their keys. These have access admin access to every
# album, and can access the special administrative interface.
admins = {
	'admin': 'imtheadmin',
}

# EDIT ONLY IF YOU USE APACHE + MOD_SCGI
# The site root, ie. if you plan to put pyctures in
# http://www.mysite.com/photos, set it to '/photos'.
# Leave it commented for all setups except apache with mod_scgi.
#root = ''

