
/*
 * General functions, taken from reddit.com
 */

function $(){
	var elements = new Array();
	for (var i = 0; i < arguments.length; i++) {
		var element = arguments[i];
		if (typeof element == 'string')
			element = document.getElementById(element);
		if (arguments.length == 1)
			return element;
		elements.push(element);
	}
	return elements;
}

/* You can't use $ from inline html code, I don't know why; so this provides
 * an alias for those cases */
var elembyid = $;

function hide() {
	for (i = 0; i < arguments.length; i++) {
		var e = $(arguments[i]);
		if (e)
			e.style.display = "none";
	}
}

function show() {
	for (i = 0; i < arguments.length; i++) {
		var e = $(arguments[i]);
		if (e)
			e.style.display = "";
	}
}

/* This one is actually mine */
function toggle_visibility(elem) {
	var e = $(elem);
	if (e) {
		if (e.style.display == "none") {
			e.style.display = "";
			return true;
		} else {
			e.style.display = "none";
			return false;
		}
	}
	return false;
}


/*
 * AJAX functions.
 */

/* Stupid IE */
xmlHttp = false;
try {
	xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
} catch (e) {
	try {
		xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
	} catch (e2) {
		xmlHttp = false;
	}
}
if (xmlHttp == false)
	var xmlHttp = new XMLHttpRequest();


function makecb(f) {
	function cb() {
		if (xmlHttp.readyState == 4) {
			hide("loading");
			var response = xmlHttp.responseText.parseJSON();
			//$("reply").value = response.toJSONString();
			if (response != "ERROR" && xmlHttp.status == 200) {
				f(response)
			} else {
				/* TODO: find a better way to report errors */
				alert("Error completing operation: " +
					response.toJSONString());
			}
		}
	}
	return cb;
}

function request(callback, url) {
	xmlHttp.open("GET", url, true);
	xmlHttp.onreadystatechange = makecb(callback);
	show("loading");
	xmlHttp.send(null);
}

/*
 * AJAX actions
 */

/* Caption */

function show_editcaption(iname) {
	if (toggle_visibility("editcaption_" + iname))
		$("cvalue_" + iname).focus();
}

function submit_editcaption(iname) {
	var album = $("album_name").value;
	var newcaption = $("cvalue_" + iname).value;

	request(captioncb,
		'caption_set?aname=' + encodeURIComponent(album) +
			'&iname=' + encodeURIComponent(iname) +
			'&caption=' + encodeURIComponent(newcaption));

	hide("editcaption_" + iname);
	return false;
}

function captioncb(resp) {
	album = resp[0];
	image = resp[1];
	caption = resp[2];

	if (caption != "") {
		$('caption_' + image).innerHTML = caption + '<br>';
	} else {
		$('caption_' + image).innerHTML = "";
	}
}


/* Description */

function show_editdesc(iname) {
	if (toggle_visibility("editdesc_" + iname))
		$("dvalue_" + iname).focus();
}

function submit_editdesc(iname) {
	var album = $("album_name").value;
	var newdesc = $("dvalue_" + iname).value;

	request(desccb,
		'desc_set?aname=' + encodeURIComponent(album) +
			'&iname=' + encodeURIComponent(iname) +
			'&desc=' + encodeURIComponent(newdesc));

	hide("editdesc_" + iname);
	return false;
}

function desccb(resp) {
	album = resp[0];
	image = resp[1];
	desc = resp[2];

	if (desc != "") {
		$('desc_' + image).innerHTML = desc + '<br>';
	} else {
		$('desc_' + image).innerHTML = "";
	}
}

/* Image rotate */
function rotate(iname, times) {
	var album = $("album_name").value;

	request(rotatecb,
		'rotate?aname=' + encodeURIComponent(album) +
			'&iname=' + encodeURIComponent(iname) +
			'&times=' + times);
}

function rotatecb(resp) {
	album = resp[0];
	image = resp[1];

	var img = $('thumb_' + image);

	/* this is so lame, but if we don't change the source, it won't
	 * reload, so we must force it this way... */
	img.src = img.src + '&x';
}


/* Album caption */

function a_show_editcaption(aname) {
	if (toggle_visibility("editcaption_" + aname))
		$("cvalue_" + aname).focus();
}

function a_submit_editcaption(aname) {
	var newcaption = $("cvalue_" + aname).value;

	request(a_captioncb,
		'album_caption_set?aname=' + encodeURIComponent(aname) +
			'&caption=' + encodeURIComponent(newcaption));

	hide("editcaption_" + aname);
	return false;
}

function a_captioncb(resp) {
	album = resp[0];
	caption = resp[1];

	if (caption != "") {
		$('caption_' + album).innerHTML = caption;
	} else {
		/* Shouldn't happen since album_caption_set returns the album
		 * title, but just in case. */
		$('caption_' + album).innerHTML = album;
	}
}


/* Album description */

function a_show_editdesc(aname) {
	if (toggle_visibility("editdesc_" + aname))
		$("dvalue_" + aname).focus();
}

function a_submit_editdesc(aname) {
	var newdesc = $("dvalue_" + aname).value;

	request(a_desccb,
		'album_desc_set?aname=' + encodeURIComponent(aname) +
			'&desc=' + encodeURIComponent(newdesc));

	hide("editdesc_" + aname);
	return false;
}

function a_desccb(resp) {
	album = resp[0];
	desc = resp[1];
	if (desc != "") {
		$('desc_' + album).innerHTML = desc + '<br>';
	} else {
		$('desc_' + album).innerHTML = "";
	}
}


/* Album featured image */

function a_set_featured(featured) {
	var album = $("album_name").value;
	request(a_featuredcb,
		'album_feat_set?aname=' + encodeURIComponent(album) +
			'&feat=' + encodeURIComponent(featured));
}

function a_featuredcb(resp) {
	album = resp[0];
	prevfeatured = resp[1];
	newfeatured = resp[2];

	if (prevfeatured)
		$("frame_" + prevfeatured).className = 'thumbframe';
	$("frame_" + newfeatured).className = 'thumbframe_feat';
}

