#coding: utf8

import string
import os
import sys
import shutil
import urllib
import math

import web
import simplejson

import config
from urls import export
import utils
import access
from access import get_user
import sessions
import backend


def get_album_image(aname, iname, user):
	try:
		album = backend.get_album(aname, user)
		image = album.get_image(iname)
		return album, image
	except:
		return None, None

# used for json string transmission
def u8dec(s):
	return s.decode('utf8')

def escape(s):
	return urllib.quote(s)


#
# Static file serving
#

class static:
	def GET(self, fname):
		if '..' in fname:
			return web.notfound()
		if not os.path.isfile('static/' + fname):
			return web.notfound()
		sys.stdout.write(open('static/' + fname).read())
export(static, '/static/(.*)')


#
# Image serving
#
# It would be faster to serve these as static files. However, it's
# also simpler to do it this way, because we make the backend data
# independant of the website. As this is not supposed to work on very
# high volume sites, it should be good enough.

class thumb:
	def GET(self):
		i = web.input('aname', 'iname')
		album, image = get_album_image(i.aname, i.iname, get_user())
		if not image:
			return web.notfound()

		# thumbs are always jpeg
		web.header('Content-type', 'image/jpeg')
		web.header('Content-disposition',
				'inline; filename="thumb-' + image.fname + '"')
		print image.openthumb().read()
export(thumb, '/thumb')

class medium:
	def GET(self):
		i = web.input('aname', 'iname')
		album, image = get_album_image(i.aname, i.iname, get_user())
		if not image:
			return web.notfound()

		web.header('Content-type', 'image/' + image.format.lower())
		web.header('Content-disposition',
				'inline; filename="' + image.fname + '"')

		print image.openmedium().read()
export(medium, '/medium')

class image:
	def GET(self):
		i = web.input('aname', 'iname')
		album, image = get_album_image(i.aname, i.iname, get_user())
		if not image:
			return web.notfound()

		web.header('Content-type', 'image/' + image.format.lower())
		web.header('Content-disposition',
				'inline; filename="' + image.fname + '"')

		print image.open().read()
export(image, '/image')


#
# Web pages
#

class index:
	def GET(self):
		user = get_user()
		albums = backend.get_album_list(user)
		albums = [ (a, a.get_featured_image(user),
				'w' in a.allowed(user)) for a in albums ]
		# albums is now a list of (album, first_image, allow_writes)
		web.render('index.html')
export(index, '/index')
export(index, '/')

class login:
	def GET(self):
		i = web.input(invalid = 0)
		web.render('login.html')

	def POST(self):
		i = web.input('user', 'passwd')
		user = utils.filter_auth(i.user)
		passwd = utils.filter_auth(i.passwd)

		if not access.auth(user, passwd):
			return web.redirect('login?invalid=1')
		sid = sessions.new(user, {'username': user})
		web.setcookie('sid', sid)
		return web.redirect("index")
export(login, '/login')

class logout:
	def GET(self):
		sid = access.get_sid()
		sessions.destroy(sid)
		web.setcookie('sid', '')
		return web.redirect('index')
export(logout, '/logout')

class album:
	def GET(self):
		i = web.input('aname', start = 0, next = -1, all = 0, page = 0)
		user = get_user()
		album = backend.get_album(i.aname, user)
		if not album:
			return web.notfound()
		images = album.list_images()
		nimages = album.count

		if i.all:
			start = 0
			next = -1
			prev = -1
			stop = nimages
			# pnumber and ptotal not necessary in this case
			pnumber = ptotal = 1
		else:
			# easy namespace to use in the template unused by now
			if i.page:
				# i.page takes precedence over i.start
				start = config.imagesxpage * (int(i.page) - 1)
			else:
				start = int(i.start)
			next = int(i.next)
			if next < 0:
				next = start + config.imagesxpage
			pnumber = start / config.imagesxpage + 1
			ptotal = int(
				math.ceil(nimages / float(config.imagesxpage)))

			# negative values are handled by the template as an
			# indication not to put a "prev" link
			prev = start - config.imagesxpage

			if next > 0: stop = next
			else: stop = start + config.imagesxpage
			images = images[start:stop]

			if next >= nimages:
				next = -1

		allow_writes = 'w' in album.allowed(user)
		allow_admin = 'a' in album.allowed(user)

		web.render('album.html')
export(album, '/album')

class details:
	def GET(self):
		i = web.input('aname', 'iname')
		user = get_user()
		album, image = get_album_image(i.aname, i.iname, user)
		if not image:
			return web.notfound()

		# some useful variables for the template
		il = album.list_images()
		prev = None
		next = None
		nprev = il.index(image) - 1
		nnext = il.index(image) + 1
		if nprev >= 0:
			prev = il[nprev]
		if nnext < len(il):
			next = il[nnext]

		# we need to put a link back to the album in the right page,
		# so we need to calculate where it will start
		# use image.number - 1 because it starts from 1, but the
		# album's page starts from 0
		n = image.number - 1
		start = max(n - n % config.imagesxpage, 0)

		allow_writes = 'w' in album.allowed(user)

		web.render('details.html')
export(details, '/details')

class admin:
	def GET(self):
		user = get_user()
		if user not in config.admins:
			return web.notfound()

		album_list = backend.get_all_album_list(user)
		msg = web.input(msg = '').msg
		return web.render('admin.html')

	def POST(self):
		user = get_user()
		if user not in config.admins:
			return web.notfound()

		album_list = backend.get_all_album_list(user)
		i = web.input('action')
		if i.action == 'reload_config':
			reload(config)
			msg = 'Configuration reloaded'
			return web.render('admin.html')
		elif i.action == 'reload_albums':
			backend.reload_albums()
			msg = 'Albums reloaded'
			return web.render('admin.html')
		else:
			raise
export(admin, '/admin')

class create_album:
	def POST(self):
		user = get_user()
		if user not in config.admins:
			return web.notfound()

		i = web.input('aname', 'parent')
		aname = utils.filter_file(i.aname)

		if i.parent == '/':
			path = config.albumspath + '/' + aname
		else:
			parent = backend.get_album(i.parent, user)
			if not parent:
				return web.notfound()
			path = parent.childrenpath + '/' + aname

		try:
			os.mkdir(path)
			os.mkdir(path + '/children')
			os.mkdir(path + '/images')
			os.mkdir(path + '/mdata')
			os.mkdir(path + '/medium')
			os.mkdir(path + '/thumbnails')
			os.mkdir(path + '/comments')
			open(path + '/users', 'w').write('all\n')
			backend.reload_albums()
			msg = "Album created successfuly"
		except OSError, desc:
			msg = "Error creating album: " + str(desc)

		album_list = backend.get_all_album_list(user)
		return web.render('admin.html')
export(create_album, '/create_album')

class upload:
	def GET(self):
		user = get_user()
		i = web.input('aname', msg = '')
		album = backend.get_album(i.aname, user)
		if (not album) or not ('a' in album.allowed(user)):
			return web.notfound()
		msg = i.msg
		return web.render('upload.html')

	def POST(self):
		user = get_user()
		i = web.input('aname', file1 = {}, file2 = {}, \
				file3 = {}, file4 = {})
		album = backend.get_album(i.aname, user)
		if (not album) or not ('a' in album.allowed(user)):
			return web.notfound()

		for field in 'file1', 'file2', 'file3', 'file4':
			if (field not in i) or (not i[field].filename):
				continue
			fd = i[field].file
			fname = utils.filter_file(i[field].filename)
			dst = open(album.imagespath + '/' + fname, 'w')
			shutil.copyfileobj(fd, dst)
			dst.close()

		backend.reload_albums()
		msg = 'Upload successful'
		return web.render("upload.html")
export(upload, '/upload')

class add_comment:
	def POST(self):
		user = get_user()
		if not user:
			return web.notfound()
		i = web.input('aname', 'iname', 'author', 'content')
		album, image = get_album_image(i.aname, i.iname, user)
		if not image:
			return web.notfound()

		if i.author and i.content:
			image.comments.append(i.author, i.content)
		return web.redirect('details?aname=%s&iname=%s' % \
					(i.aname, i.iname))
export(add_comment, '/add_comment')


#
# AJAX functions
#

class caption_set:
	def GET(self):
		i = web.input('aname', 'iname', 'caption')
		user = get_user()
		album, image = get_album_image(i.aname, i.iname, user)
		allow_writes = 'w' in album.allowed(user)
		if not image or not allow_writes:
			return web.notfound()

		image.mdata.caption = i.caption
		image.mdata.save()
		print simplejson.dumps([i.aname, i.iname, u8dec(i.caption)])
export(caption_set, '/caption_set')

class desc_set:
	def GET(self):
		i = web.input('aname', 'iname', 'desc')
		user = get_user()
		album, image = get_album_image(i.aname, i.iname, user)
		allow_writes = 'w' in album.allowed(user)
		if not image or not allow_writes:
			return web.notfound()

		image.mdata.desc = i.desc
		image.mdata.save()
		print simplejson.dumps([i.aname, i.iname, u8dec(i.desc)])
export(desc_set, '/desc_set')

class rotate:
	def GET(self):
		i = web.input('aname', 'iname', 'times')
		user = get_user()
		album, image = get_album_image(i.aname, i.iname, user)
		allow_writes = 'w' in album.allowed(user)
		if not image or not allow_writes:
			return web.notfound()

		try:
			times = int(i.times)
		except:
			print simplejson.dumps('ERROR')
			return
		image.rotate(times)
		print simplejson.dumps([i.aname, i.iname])
export(rotate, '/rotate')

class album_caption_set:
	def GET(self):
		i = web.input('aname', 'caption')
		user = get_user()
		album = backend.get_album(i.aname, user)
		allow_writes = 'w' in album.allowed(user)
		if not album or not allow_writes:
			return web.notfound()

		album.mdata.caption = i.caption
		album.mdata.save()
		print simplejson.dumps([i.aname,
				u8dec(album.get_title())])
export(album_caption_set, '/album_caption_set')

class album_desc_set:
	def GET(self):
		i = web.input('aname', 'desc')
		user = get_user()
		album = backend.get_album(i.aname, user)
		allow_writes = 'w' in album.allowed(user)
		if not album or not allow_writes:
			return web.notfound()

		album.mdata.desc = i.desc
		album.mdata.save()
		print simplejson.dumps([i.aname, u8dec(i.desc)])
export(album_desc_set, '/album_desc_set')

class album_feat_set:
	def GET(self):
		i = web.input('aname', 'feat')
		user = get_user()
		album = backend.get_album(i.aname, user)
		allow_writes = 'w' in album.allowed(user)
		if not album or not allow_writes:
			return web.notfound()

		if album.featured:
			prev = album.featured.fname
		else:
			prev = None
		album.set_featured_image(i.feat)
		print simplejson.dumps([i.aname, prev, i.feat])
export(album_feat_set, '/album_feat_set')


