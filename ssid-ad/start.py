#!/usr/bin/python
import sys
import time
import paramiko

TARGET="192.168.2.10"
PORT=22
USER="root"
PASSWORD="iii"
PRE_COMMAND="/sbin/ifconfig ath1 up"
COMMAND="/usr/sbin/iwconfig ath1 essid '%s'"
SET_MAC="/sbin/ifconfig ath1 down hw ether %s up"
POST_COMMAND="/sbin/ifconfig ath1 down"

#ASCII!!!
TEXT="""Teil I
Wirklich, ich lebe in finsteren Zeiten! 
Das arglose Wort ist toericht. Eine glatte Stirn 
Deutet auf Unempfindlichkeit hin. Der Lachende 
Hat die furchtbare Nachricht 
Nur noch nicht empfangen. 

Was sind das fuer Zeiten, wo 
Ein Gespraech ueber Baeume fast ein Verbrechen ist 
Weil es ein Schweigen ueber so viele Untaten einschliesst! 
Der dort ruhig ueber die Strasse geht 
Ist wohl nicht mehr erreichbar fuer seine Freunde 
Die in Not sind? 

Es ist wahr: Ich verdiene nur noch meinen Unterhalt 
Aber glaubt mir: das ist nur ein Zufall. Nichts 
Von dem, was ich tue, berechtigt mich dazu, mich sattzuessen. 
Zufaellig bin ich verschont. (Wenn mein Glueck aussetzt, bin ich verloren.) 

Man sagt mir: Iss und trink du! Sei froh, dass du hast! 
Aber wie kann ich essen und trinken, wenn 
Ich dem Hungernden entreisse, was ich esse, und 
Mein Glas Wasser einem Verdursteten fehlt? 
Und doch esse und trinke ich. 

Ich waere gerne auch weise. 
In den alten Buechern steht, was weise ist: 
Sich aus dem Streit der Welt halten und die kurze Zeit 
Ohne Furcht verbringen 
Auch ohne Gewalt auskommen 
Boeses mit Gutem vergelten 
Seine Wuensche nicht erfuellen, sondern vergessen 
Gilt fuer weise. 
Alles das kann ich nicht: 
Wirklich, ich lebe in finsteren Zeiten! 


Teil II 
In die Staedte kam ich zur Zeit der Unordnung 
Als da Hunger herrschte. 
Unter die Menschen kam ich zu der Zeit des Aufruhrs 
Und ich empoerte mich mit ihnen. 
So verging meine Zeit 
Die auf Erden mir gegeben war. 

Mein Essen ass ich zwischen den Schlachten 
Schlafen legte ich mich unter die Moerder 
Der Liebe pflegte ich achtlos 
Und die Natur sah ich ohne Geduld. 
So verging meine Zeit 
Die auf Erden mir gegeben war. 

Die Strassen fuehrten in den Sumpf zu meiner Zeit. 
Die Sprache verriet mich dem Schlaechter. 
Ich vermochte nur wenig. Aber die Herrschenden 
Sassen ohne mich sicherer, das hoffte ich. 
So verging meine Zeit 
Die auf Erden mir gegeben war. 

Die Kraefte waren gering. Das Ziel 
Lag in grosser Ferne 
Es war deutlich sichtbar, wenn auch fuer mich 
Kaum zu erreichen. 
So verging meine Zeit 
Die auf Erden mir gegeben war. 


Teil III 
Ihr, die ihr auftauchen werdet aus der Flut 
In der wir untergegangen sind 
Gedenkt 
Wenn ihr von unseren Schwaechen sprecht 
Auch der finsteren Zeit 
Der ihr entronnen seid. 

Gingen wir doch, oefter als die Schuhe die Laender wechselnd 
Durch die Kriege der Klassen, verzweifelt 
Wenn da nur Unrecht war und keine Empoerung. 

Dabei wissen wir doch: 
Auch der Hass gegen die Niedrigkeit 
Verzerrt die Zuege. 
Auch der Zorn ueber das Unrecht 
Macht die Stimme heiser. Ach, wir 
Die wir den Boden bereiten wollten fuer Freundlichkeit 
Konnten selber nicht freundlich sein. 

Ihr aber, wenn es soweit sein wird 
Dass der Mensch dem Menschen ein Helfer ist 
Gedenkt unsrer 
Mit Nachsicht."""

###############
elements = TEXT.split("\n")
for i in range(elements.__len__()):
	elements[i] = "%d - %s" % (i, elements[i])


def set_mac(transport, id):
	mac = 0x101884153480 + id
	mac_str = "%x:%x:%x:%x:%x:%x" % (
					(mac & 0xff0000000000) >> 40,
					(mac & 0x00ff00000000) >> 32,
					(mac & 0x0000ff000000) >> 24,
					(mac & 0x000000ff0000) >> 16,
					(mac & 0x00000000ff00) >> 8,
					(mac & 0x0000000000ff) >> 0)
	cmd = SET_MAC % mac_str
	print cmd
	transport.open_session().exec_command(cmd)
	
def set_ssid(transport, text):
	print COMMAND%text
	transport.open_session().exec_command(COMMAND % text)

t = paramiko.Transport((TARGET,PORT))
t.connect(username=USER, password=PASSWORD)

print PRE_COMMAND
t.open_session().exec_command(PRE_COMMAND)

try:
	while 1:
		for i in range(elements.__len__()):
			set_mac(t, i)
			set_ssid(t, elements[i])
			time.sleep(0.5)
finally:
	print "POST_COMMAND: %s" % POST_COMMAND
	t.open_session().exec_command(POST_COMMAND)
	t.close()



